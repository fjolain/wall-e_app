package com.fjolain.walle_app;

import android.util.Log;

public class Controler {
	
	public static byte ID_LEFT_EYE = 1;
	public static byte ID_RIGHT_EYE = 2;
	public static byte ID_LEFT_ARM = 3;
	public static byte ID_RIGHT_ARM = 4;
	public static byte ID_SPEED = 5;
	
	private int meanLeftSpeed = 0;
	private int meanRightSpeed = 0;
	private int length = 5;
	private int iteration = 0;
	

	private BluetoothConnect bluetooth;
	
	public Controler() {
		
	}
	
	public void setBluetooth(BluetoothConnect bluetooth) {
		this.bluetooth = bluetooth;
	}
	
	
	public void updateLeftArm(int value) {
		byte[] array = {ID_LEFT_ARM, (byte) value, 0};
		bluetooth.send(array);
	}
	
	public void updateRightArm(int value) {
		byte[] array = {ID_RIGHT_ARM, (byte) value, 0};
		bluetooth.send(array);
	}
	
	public void updateLeftEye(int value) {
		byte[] array = {ID_LEFT_EYE, (byte) value, 0};
		bluetooth.send(array);
	}
	
	public void updateRightEye(int value) {
		byte[] array = {ID_RIGHT_EYE, (byte) value, 0};
		bluetooth.send(array);
	}
	
	public void updateRotation(float alpha, float beta, float gamma) {
		
		
		if (iteration <= 0) {
			
			int leftSpeed = meanLeftSpeed/length;
			int rightSpeed = meanRightSpeed/length;
			
			// Limit value
			leftSpeed = Math.max(leftSpeed, 0);
			leftSpeed = Math.min(leftSpeed, 255);
			rightSpeed = Math.max(rightSpeed, 0);
			rightSpeed = Math.min(rightSpeed, 255);
		
			byte[] array = {ID_SPEED, (byte) leftSpeed, (byte) rightSpeed};
			bluetooth.send(array);
			
			iteration = length;
			meanLeftSpeed = 0;
			meanRightSpeed = 0;
		}
		
		else {
			
			int acceleration = (int) (-gamma*150);
			int wheel = (int) (beta*100);
			
			meanLeftSpeed += acceleration -wheel;
			meanRightSpeed += acceleration + wheel;
			
			iteration--;
		}
	}

}
