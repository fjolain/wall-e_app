package com.fjolain.walle_app;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;

public class BluetoothConnect extends Thread{
	
	private BluetoothSocket mmSocket;
	private final BluetoothDevice mmDevice;
	private OutputStream out;
	
	private boolean enable = false;
	
	private static BluetoothConnect instance;
 
	private BluetoothConnect(BluetoothDevice device) {
		mmDevice = device;
		try {
			UUID uuid = UUID.fromString(device.getUuids()[0].toString());
			mmSocket = device.createRfcommSocketToServiceRecord(uuid);
			mmSocket.connect();
			out = mmSocket.getOutputStream();
			enable = true;
		} catch (IOException e) { 
			e.printStackTrace();
		}
	}
	
	public static BluetoothConnect getInstance(BluetoothDevice device) {
		if (instance == null)
			instance = new BluetoothConnect(device);
		return instance;
	}
	
	public static BluetoothConnect getInstance() {
		return instance;
	}
 
	public void send(byte[] array) {
		try {
			out.write(array);
			out.flush();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	public void cancel() {
		try {
			mmSocket.close();
			enable = false;
		} catch (IOException e) { }
	}
	
	public boolean isEnable() {
		return enable;
	}
   
}

