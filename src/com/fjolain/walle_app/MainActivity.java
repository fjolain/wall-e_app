package com.fjolain.walle_app;

import java.util.Set;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.VerticalSeekBar;

public class MainActivity extends Activity {
	
	private VerticalSeekBar leftArm;
	private VerticalSeekBar rightArm;
	private RadioGroup leftEye;
	private RadioGroup rightEye;
	private TextView text;
	
	private BluetoothAdapter myBluetooth;
	private BluetoothConnect bluetoothConnect;
	
	private SensorManager sensorManager;
	private Sensor accelerometer;
	private Sensor magnetometer;
	private MySensorListener mySensorListener;
	
	private float rotation[];
	private float accelerometerValues[];
	private float magnetometerValues[];
	
	private Controler controler = new Controler();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// Use landscape
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		
		leftArm = (VerticalSeekBar) findViewById(R.id.left_arm);
		rightArm = (VerticalSeekBar) findViewById(R.id.right_arm);
		leftEye = (RadioGroup) findViewById(R.id.left_eyes);
		rightEye = (RadioGroup) findViewById(R.id.right_eyes);
		text = (TextView) findViewById(R.id.text);

		// Setup Sensor
	    sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
	    accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	    mySensorListener = new MySensorListener();
	    sensorManager.registerListener(mySensorListener, accelerometer, SensorManager.SENSOR_DELAY_UI);
	    sensorManager.registerListener(mySensorListener, magnetometer, SensorManager.SENSOR_DELAY_UI);
	    
	    // Setup VerticalSeekBar
	    leftArm.setMax(100);
	    leftArm.incrementProgressBy(1);
	    leftArm.setProgress(100);
	    leftArm.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean arg2) {
				controler.updateLeftArm(progress);
				
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
	    	
	    });
	    rightArm.setMax(100);
	    rightArm.incrementProgressBy(1);
	    rightArm.setProgress(100);
	    rightArm.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean arg2) {
				controler.updateRightArm(progress);
				
			}

			@Override
			public void onStartTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onStopTrackingTouch(SeekBar arg0) {
				// TODO Auto-generated method stub
				
			}
	    	
	    });
	    
	    // Setup RadioGroup
	    leftEye.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup arg0, int id) {
				if(id == R.id.left_eye_open)
					controler.updateLeftEye(1);
				else if(id == R.id.left_eye_close)
					controler.updateLeftEye(0);
				
			}
	    	
	    });
	    rightEye.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(RadioGroup arg0, int id) {
				if(id == R.id.right_eye_open)
					controler.updateRightEye(1);
				else if(id == R.id.right_eye_close)
					controler.updateRightEye(0);
				
			}
	    	
	    });
	    
	    // Get the bluetooth
	    myBluetooth = BluetoothAdapter.getDefaultAdapter();
	    
	    // Enable bluetooth
	    if(!myBluetooth.isEnabled())
	    	myBluetooth.enable();
	    
	    // Select the correct device
	    Set<BluetoothDevice> devices = myBluetooth.getBondedDevices();
	    for (BluetoothDevice device : devices) {
	    	  if(device.getName().equals("HC-05")) {
	    		  Log.v("", "HC-05 find");
	    		  bluetoothConnect = BluetoothConnect.getInstance(device);
	    		  controler.setBluetooth(bluetoothConnect);
	    	  }
	    	  
	    
	    }
	    
	    
	    
	}
	
	@Override
	public void onResume() {
		super.onResume();
				
		sensorManager.registerListener(mySensorListener, accelerometer, SensorManager.SENSOR_DELAY_UI);
	    sensorManager.registerListener(mySensorListener, magnetometer, SensorManager.SENSOR_DELAY_UI);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		
		sensorManager.unregisterListener(mySensorListener);
	}
	
	
	
	
	
	/**
	 * Implements SensorEventListener.
	 * Used to gathering new rotation sensor values.
	 * @author francois
	 *
	 */
	private class MySensorListener implements SensorEventListener {

		@Override
		public void onAccuracyChanged(Sensor arg0, int arg1) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onSensorChanged(SensorEvent se) {
			
			
			// Filter event by sensor actived
			if(se.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
				accelerometerValues = se.values;
			if(se.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
				magnetometerValues = se.values;
			
			// Update rotation of the screen's phone
			rotation = new float[3];
			float[] R = new float[9];
			float[] I = new float[9];
			if (accelerometerValues != null && magnetometerValues != null) {
				SensorManager.getRotationMatrix(R, I, accelerometerValues, magnetometerValues);
				SensorManager.getOrientation(R, rotation);

				// Send new rotation to the controler
				controler.updateRotation(rotation[0], rotation[1], rotation[2]);
				
			}
			
		}
		
	}
	
}
